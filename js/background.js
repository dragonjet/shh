/* APP SETTINGS
------------------------------------*/
var default_options = {
	screen_size: 2,
	TL_inline: 1,
	TL_image: 1,
	ctouch: 1,
	timer_screen: 1,
	timer_sound: 1
};

for(i in default_options){
	if(typeof localStorage[i] == "undefined"){
		localStorage[i] = default_options[i];
	}
}


/* EXTENSION CALLS
------------------------------------*/
chrome.runtime.onMessage.addListener(function(request, sender, response) {
    if(request.game==="shirohime"){
		switch(request.action){
			case "translations":
				response({
					enabled:{
						inline: localStorage['TL_inline'],
						image: localStorage['TL_image']
					},
					translations: translations,
					elements: elements
				});
				break;
			case "inlineScript":
				if(localStorage['ctouch']==1){
					response({source: inlineScript});
				}else{
					response({source:""});
				}
				break;
			default:
				break;
		}
	}
});


/* LOAD DATA
------------------------------------*/
var translations = {};
var elements = "";
var inlineScript = "";
var tmpTranslations = {};
var breakWikiaCache = Math.floor(new Date().getTime()/60000);
var stripDiv = document.createElement('div');

function loadTranslations(wikiaArticleId, wikiaArticleName){
	$.ajax({
		url:'http://shirohime.wikia.com/wiki/User:Dragonjet/Translations/'+wikiaArticleName+'?action=render',
        jsonp: false,
		success:function(data){
			stripDiv.innerHTML = data;
			$.extend(translations, JSON.parse(stripDiv.textContent));
		}
	});
}

$(function(){
	// get elements
	$.ajax({
		url:'http://shirohime.wikia.com/wiki/User:Dragonjet/Translations/Elements?action=render',
        jsonp: false,
		success:function(data){
			stripDiv.innerHTML = data;
			elements = stripDiv.textContent;
		}
	});
	
	// get translations
	loadTranslations(3033, "Castles");
	loadTranslations(3034, "Quests");
	loadTranslations(3035, "Other");
	loadTranslations(3036, "Messages");
	loadTranslations(3037, "Info");
	loadTranslations(3038, "Affairs");
	
	// get ctouch code
	$.get('../inject/touch.js', function(data){
		inlineScript = data;
	}, 'text');
});


/*INTERCEPT
------------------------------------*/
chrome.webRequest.onBeforeSendHeaders.addListener(function(details){
		var headers = details.requestHeaders,
		blockingResponse = {};
		for( var i = 0, l = headers.length; i < l; ++i ) {
			if( headers[i].name == 'User-Agent' ) {
				headers[i].value = 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0_4 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11B554a Safari/9537.53';
				break;
			}
		}
		blockingResponse.requestHeaders = headers;
		return blockingResponse;
	},
	{ urls: ["*://a60462.app.gree-pf.net/*", "*://*.gree.net/*"]},
	['requestHeaders','blocking']
);